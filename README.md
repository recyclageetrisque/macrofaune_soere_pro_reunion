# Project name : Macroinvertebrates Diversity Analysis - SOERE PRO REUNION

## Description

This repository contains the R script and associated materials for analyzing the diversity of macroinvertebrates in soil and litter from a 10-year field trial on fertilization in a sugarcane agroecosystem (SOERE PRO Reunion). 

The analysis supports the paper: "Does fertilizer type drive soil and litter macroinvertebrate communities in a sugarcane agroecosystem? Evidence from a 10-year field trial" published in Agriculture, Ecosystem and Environment.

DOI : 10.1016/j.agee.2024.109431

Estelle Jacquin1, 2, 3, Marie-Liesse Vermeire2, 3, 4*, Eric Blanchart5, Charles Detaille2, 3, 6, François-Régis Goebel7, 8, Janine Jean7,8, Malalatiana Razafindrakoto9, Matthieu N. Bravin2, 3

* Corresponding Author

1 UniLaSalle, AGHYLE UP 2018.C101, 60000 Beauvais, France 

2 CIRAD, UPR Recyclage et Risque, F 34398 Montpellier, France

3 Recyclage et Risque, Univ Montpellier, CIRAD, F‑34398 Montpellier, France

4 IESOL, IRD-ISRA, Bel-Air Center, P.O. Box 1386, P.C 18524, Dakar, Senegal

5 Eco&Sols, Univ Montpellier, IRD, INRAe, CIRAD, Montpellier SupAgro, Montpellier, France

6 CIRAD, UPR Recyclage et Risque, F-97743 Saint-Denis, Réunion, France

7 CIRAD, UPR Agroecologie et Intensification Durable des cultures Annuelles, F-34398 Montpellier, France

8 Agroecologie et Intensification Durable des cultures Annuelles, Univ Montpellier, CIRAD, F-34398 Montpellier, France

9 Laboratoire des RadioIsotopes, Université d’Antananarivo, Madagascar



## Features of the Script:

*Data cleaning and preprocessing*: Ensures the dataset is ready for statistical analysis

*Statistical analysis*: Explores the effects of fertilization on macroinvertebrate diversity in soil and litter. The analysis strategy was designed with the help of Anastats.

*Publication-ready figures and tables*: Outputs include visualizations and tables formatted for inclusion in scientific publications.

## Dataset

The dataset supporting this analysis is freely available on Dataverse:

Replication Data:

Jacquin, E., Vermeire, M.-L., Detaille, C., Bravin, M., 2024. Replication Data for: Does fertilization type drive soil and litter macroinvertebrate communities in a sugarcane agroecosystem? Evidence from a 10-year field trial. CIRAD dataverse, v1

DOI : https://doi.org/10.18167/DVN1/VDUXQP


## Requirements

### R Environment

*R version* : 4.2.2 (2022-10-31)

## Contact

For questions or suggestions, please contact:

*Marie-Liesse Vermeire*
Institution: CIRAD
Email: marie-liesse.vermeire@cirad.fr


## License

/***************************************************************************
*                                CIRAD
*                          PERSYST Department
*                    Research Unit Recycling & Risk
*                       University of Montpellier
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.

***************************************************************************/
